import React from 'react'
import { connect } from 'react-redux'

import { increment, decrement } from './actions'

const App = ({ message, counter, dispatch }) => (
  <div>
    <h1>counter : {counter}</h1>
    <br />
    <button onClick={() => dispatch(increment(1))}>+</button>
    <button onClick={() => dispatch(decrement(1))}>-</button>
  </div>
)

const mapStateToProps = state => ({
  counter: state.counters || 0
})

export default connect(mapStateToProps)(App)
